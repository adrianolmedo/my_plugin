# README #

This is a basic plugin for osclass, change name of main folder to "my_plugin" to be installable and work!

### What is this repository for? ###

* Require: Osclass 3.9.0 or menor

### Wiki ###

[How to develop a plugin for Osclass (Quick and complete guide)](https://bitbucket.org/adrianolmedo/my_plugin/wiki/Home)

[DataTable in Osclass (explained)](https://bitbucket.org/adrianolmedo/my_plugin/wiki/DataTable-in-Osclass)